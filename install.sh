helm install -f chart-values/redis-values.yaml rediscart charts/redis

helm install -f chart-values/email-service-values.yaml emailservice charts/microservice
helm install -f chart-values/cart-service-values.yaml cartservice charts/microservice
helm install -f chart-values/currency-service-values.yaml currencyservice charts/microservice
helm install -f chart-values/payment-service-values.yaml paymentservice charts/microservice
helm install -f chart-values/recommendation-service-values.yaml recommendationservice charts/microservice
helm install -f chart-values/productcatalog-service-values.yaml productcatalogservice charts/microservice
helm install -f chart-values/shipping-service-values.yaml shippingservice charts/microservice
helm install -f chart-values/ad-service-values.yaml adservice charts/microservice
helm install -f chart-values/checkout-service-values.yaml checkoutservice charts/microservice
helm install -f chart-values/frontend-values.yaml frontendservice charts/microservice
